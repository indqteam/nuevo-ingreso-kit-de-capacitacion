# ¡Bienvenido!

Esta es la wiki para el kit de capacitación del personal de IndQ.
Esperamos sea de fácil uso

En ella encontrarás lo siguiente

[TOC]



## La empresa
En IndQ buscamos revolucionar las industrias del futuro, proprcionando soluciones que integran el mundo físico con el digital. IndQ significa industria cuatro, que es la industria del internet de las cosas (ioT).

## La forma de trabajo

* Áreas de la empresa: En IndQ nuestro equipo de trabajo se compone de dos áreas base electrónica/hardware y sistemas/software, esto no quiere decir que trabajamos separados, para todo proyecto trabajamos en conjunto, es de nuestra preferencia formar equipos multidisciplinarios que enriquezcan los proyectos.
* Trabajo en equipo: Para enriquecer los proyectos varolamos mucho la interacción y discusiones sobre ideas y nos agrada mucho que si alguien tiene algún problema, el equipo de trabajo se apoye para resolverlo.
* Uso de herramientas: Para facilitar el trabajo en equipo de manera proactiva y productiva, utilizamos varias herramientas para la comunicación y organización de trabajo.

## Kit de trabajo
* @nadialrm te dará tu correo electrónico con el que te comunicarás para asuntos relacionados con seguimiento de los proyectos, con el fin de tener documentado evidencia de nuestras acciones. 
* Acceso a sisnology: Cloud Station para acceder a documentación compartida de la empresa y los proyectos, además de acceso en la nube a tus propios archivos. También @nadialrm te lo proporcionará.
* Uso de trello para organización de trabajo. Tu acceso a trello y los boards que necesites te los dará @dalsscarlon con el correo de la empresa.
* Para una mejor comunicación y más fluida entre el equipo usamos Slack. La invitación es al equipo indq.slack.com y esa también te la hará llegar @dalsscarlon.
* Permisos a los repositorios de proyectos en los que estes involucrado, estos repositorios estan en [bitbucket](https://bitbucket.org), con tu correo se te darán permisos a los proyectos en los que se te integré.
* Al correo te llegará las instrucciones de como usar el Cloud Station (sisnology) y las instrucciones para configurar el correo en un cliente de correo. 

Ya casi estas listo para trabajar, ahora solo revisa tu correo [aquí](http://indqtech.com/webmail) y crea tus cuentas.

## Equipos de desarrollo, uso de herramientas base


### Trello workflow para developers

Para el uso de Trello utilizamos las siguiente [metodología](http://buildbettersoftware.com/trello-for-software-development) basada en 6 tarjetas que constan desde lo que se tiene que hacer hasta las cosas que ya están en uso.

### Que es Slack

Slack es una herramienta de mensajería pensada para equipos. Al igual que otras opciones existentes permite crear grupos a los que accederán los diferentes miembros para poder estar comunicados todos, como si estuviesen en la misma oficina. Pero claro, visto así poco interés tendría como para que todo el mundo esté usándola y hablando de ella.

Las ventajas y puntos fuertes de Slack es que no sólo permite crear grupos, conversaciones privadas, canales que serían como salas más generales a las que todos se pueden unir, compartir enlaces o archivos adjuntos, etc… sino que es un servicio altamente configurable.

[Funcionalidad de Slack](https://slack.com/features)

Dentro de Slack esta un bot llamado @miamigodaily, el que te estará preguntando diariamente sobre el trabajo que realizas y realizarás esto solo con el fin de tener a los lideres de proyectos informados del trabajo diario.

### Cloud Station
Para instalar su estacion de trabajo:

	Windows: Explorador de archivos : \\indQcloud 
    MAC: Navegador: smb://indQcloud
    ubuntu: en archivos,  RED encontrarán acceso al INDQCLOUD


En cualquiera de las opciones les va a pedir usuario y contraseña, es el que les llegó a su correo.

Dicho acceso es para la información que compartimos de los proyectos, hay una carpeta de Documentación base ahi está el logo, wallpapers de indQ y archivos base para crear documentación, pueden sugerir alguno otro que crean sea necesario.


Cada quien tiene acceso a los proyectos en los cuales esta trabajando, si a alguno le hace falta algun acceso por favor avisen a @nadialrm
La otra es que todos cuentan con un espacio de almacenamiento del cloud de indQ, ese lo pueden usar como el Drive de google. Son cuentas independientes... y uds deciden si dan acceso a ciertos archivos a los usuarios del cloud. 
La applicación para usar CloudStation la pueden descargar de [aqui](https://www.synology.com/en-us/support/download/DS216j#utilities) (Cloud Station Drive) viene para Windows Ubuntu Fedora Mac 

    Les pedirá 3 datos: el primero es el grupo indqtech , los otros dos son usuario y contraseña de cada quien.


Igual, pueden utilizar el CloudStation desde Web: [indqtech.quickconnect.to](indqtech.quickconnect.to)

### GIT

Para el control de versiones de cualquier proyecto se hace uso de GIT como sistema de control de versiones, en [este enlace](https://try.github.io/levels/1/challenges/1) encontrarás la información para el uso de herramientas base a manera de práctica o guía didáctica.

Si quieres conocer un poco mas a fondo https://git-scm.com/


### Bitbucket 

Nuestros repositorios estan hosteados en Bitbucket. Dentro de cada repositorio podrás encontrar una Wiki acerca del proyecto y alguna documentación de apoyo, aquí mismo será donde podrás enriquecer esta documentación con tu trabajo. 
El flujo de trabajo que usamos es el siguiente: [flujo](https://datasift.github.io/gitflow/IntroducingGitFlow.html)

Para más información de como usar el bitbucket [aquí lo puedes ver](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)


Have fun!

